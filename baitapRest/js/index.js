const tinhDTB = (...nums) => {
    let input = 0;
    let count = 0;
    for (let i = 0; i < nums.length; i++) {
      input += nums[i];
      count++;
    }
    if (count == 3) {
      document.getElementById("tbKhoi1").innerHTML = input / count;
    } else {
      document.getElementById("tbKhoi2").innerHTML = input / count;
    }
  };
  const layThongTinTuForm1 = () => {
    let diemToan = document.getElementById("inpToan").value * 1;
    let diemLy = document.getElementById("inpLy").value * 1;
    let diemHoa = document.getElementById("inpHoa").value * 1;
    tinhDTB(diemToan, diemLy, diemHoa);
  };
  
  const layThongTinTuForm2 = () => {
    let diemVan = document.getElementById("inpVan").value * 1;
    let diemSu = document.getElementById("inpSu").value * 1;
    let diemDia = document.getElementById("inpDia").value * 1;
    let diemAnh = document.getElementById("inpEnglish").value * 1;
    tinhDTB(diemVan, diemSu, diemDia, diemAnh);
  };
  