let heading = document.querySelector(".heading");

const jump = (e) => {
  let chars = [...e];
  return chars.map((e) => `<span>${e}</span>`).join("");
};

heading.innerHTML = jump(heading.innerText);
