// Render buttons color
const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
contentHTML = ``;
colorList.forEach((e) => {
  contentHTML += `
     <button id='${e}' class="color-button ${e}" onclick='addColor("${e}")'></button>
    `;
});
document.getElementById(`colorContainer`).innerHTML = contentHTML;

// Add button đầu tiên class `active`
document.getElementsByClassName(`color-button`)[0].classList.add(`active`);

let btns = document.querySelectorAll(".color-button");
let house = document.querySelectorAll(".house");
console.log("house: ", house);
function resetButtons() {
  btns.forEach((button) => {
    if (button.classList.contains(`active`)) {
      button.classList.remove(`active`);
    }
  });
  house.forEach((house) => {
    house.classList.remove(house.classList[1]);
  });
}

function addColor(e) {
  resetButtons();
  document
    .getElementsByClassName(`color-button ${e}`)[0]
    .classList.add(`active`);

  btns.forEach((item) => {
    if (item.classList.contains(`active`)) {
      document.getElementById(`house`).classList.add(item.classList[1]);
    }
  });
}
